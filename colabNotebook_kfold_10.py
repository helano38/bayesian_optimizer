import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1"

import tensorflow as tf
from keras import models
from keras import layers
from keras import optimizers
from keras import regularizers
from keras.applications import VGG16
from keras.applications.densenet import DenseNet121
from keras.applications import ResNet50
from keras.applications import Xception
from keras.preprocessing.image import ImageDataGenerator
#from sklearn.metrics import roc_auc_score
from keras.models import load_model, Model
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense, Flatten, Dropout
import keras.layers
import pickle
import numpy as np

######Parametros#######################

image_size = 256
batch_size_train = 10
batch_size_validacao = 100
epochs_ = 30

dir_0 = 'dataset/k_fold_10/0/'
dir_1 = 'dataset/k_fold_10/1/'
dir_2 = 'dataset/k_fold_10/2/'
dir_3 = 'dataset/k_fold_10/3/'
dir_4 = 'dataset/k_fold_10/4/'
dir_5 = 'dataset/k_fold_10/5/'
dir_6 = 'dataset/k_fold_10/6/'
dir_7 = 'dataset/k_fold_10/7/'
dir_8 = 'dataset/k_fold_10/8/'
dir_9 = 'dataset/k_fold_10/9/'

resultado=[0.0, 0.0]

# validation_dir = 'deeplearningsysu/dataset/down_sized_images/validate/'

path_save = 'deeplearningsysu/dataset/'

# get base model
base_model = VGG16(weights='imagenet', include_top=False, input_shape=(image_size, image_size, 3))


def get_early_callback():
    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss',
                                  min_delta=0,
                                  patience=2,
                                  verbose=0, mode='auto')

    return early_stop

def get_generator(folder, datagen):
    image_size = 256
    batch_size_train = 10
    generator = datagen.flow_from_directory(
        folder,
        target_size=(image_size, image_size),
        batch_size=batch_size_train,
        class_mode='categorical'
    )
    return generator


def get_model():
    image_size = 256
    batch_size_train = 10
    batch_size_validacao = 10
    epochs_ = 30
    base_model = VGG16(weights='imagenet', include_top=False, input_shape=(image_size, image_size, 3))

    # build top model
    x = Flatten(name='flatten')(base_model.output)
   # x = Dropout(0.2)(x)
    x = Dense(1024, activation='relu', name='fc1')(x)
    #x = Dropout(0.2)(x)
    x = Dense(256, activation='relu', name='fc2')(x)
    #x = Dropout(0.2)(x)
#    x = Dense(64, activation='relu', name='fc3')(x)
 #   x = Dropout(0.2)(x)
    x = Dense(3, activation='softmax', name='predictions')(x)

    # stitch together
    model = Model(inputs=base_model.input, outputs=x)
    model.compile(loss='categorical_crossentropy',
                  optimizer=optimizers.RMSprop(lr=0.00001),
                  metrics=['acc'])
    #model.summary()
    return model


def fit_generator(generator, modelo, generator_validate, epochs):
    batch_size_train = 10
    batch_size_validacao = 10
    epochs_ = 1000
    early_stop = get_early_callback()
    modelo.fit_generator(
        generator,
        callbacks=[early_stop],
        validation_data=generator_validate,
        validation_steps=generator_validate.samples / generator_validate.batch_size,
        steps_per_epoch=generator.samples / generator.batch_size,
        epochs=epochs_,
        verbose=1
        # class_weight=[0.25,0.75]
    )
    return modelo


def evaluate(generator, modelo):
    batch_size_train = 10
    batch_size_validacao = 10
    epochs_ = 1000
    early_stop = get_early_callback()
    score = modelo.evaluate_generator(generator,
                                     steps=generator.samples / generator.batch_size,
                                     max_queue_size=10,
                                     verbose=1)
    resultado[0]+=score[0]
    resultado[1] += score[1]
    return score



def run_kfold_validation(folders, k, generators):
    print("++++++++++++++++++++++++++++++++++++++++++++++++++ executando fold", k)
    modelo = get_model()
    for f in folders:


        if (f != k):
            fit_generator(generators[f], modelo, generators[k])
        if (f==k):
            score = evaluate(generators[f], modelo)
            print("RESULTADO: ", score)
            with open('resultado_fold_'+str(k)+'.txt', 'w') as arquivo:
                arquivo.write(str(score[0]) + ';\t' + str(score[1]) + '\n')
            score = None

# inspect



train_datagen = ImageDataGenerator(
    samplewise_center=True,
    samplewise_std_normalization=True,
    zoom_range=[0.8, 1.2],
    horizontal_flip=True,
    rotation_range=2,
    brightness_range=[0.2, 1.0]
)

# generator_0 = get_generator(dir_0, train_datagen)
# generator_1 = get_generator(dir_1, train_datagen)
# generator_2 = get_generator(dir_2, train_datagen)
# generator_3 = get_generator(dir_3, train_datagen)
# generator_4 = get_generator(dir_4, train_datagen)
# generator_5 = get_generator(dir_5, train_datagen)
# generator_6 = get_generator(dir_6, train_datagen)
# generator_7 = get_generator(dir_7, train_datagen)
# generator_8 = get_generator(dir_8, train_datagen)
# generator_9 = get_generator(dir_9, train_datagen)

# generators = [generator_0, 
#              generator_1, 
#              generator_2,
#              generator_3, 
#              generator_4,
#              generator_5,
#              generator_6,
#              generator_7,
#              generator_8,
#              generator_9]

# # print(generators)
# # Compile the model


# # checkpoint = ModelCheckpoint('deeplearningsysu/dataset/256_epochs_dropout_50_50_fine_{epoch:03d}.h5',period=1)

# k_fold_executor_0 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# k_fold_executor_1 = [0, 1, 2, 3, 4, 5, 6, 7, 9, 8]
# k_fold_executor_2 = [0, 1, 2, 3, 4, 5, 6, 9, 8, 7]
# k_fold_executor_3 = [0, 1, 2, 3, 4, 5, 9, 7, 8, 6]
# k_fold_executor_4 = [0, 1, 2, 3, 4, 9, 6, 7, 8, 5]
# k_fold_executor_5 = [0, 1, 2, 3, 9, 5, 6, 7, 8, 4]
# k_fold_executor_6 = [0, 1, 2, 9, 4, 5, 6, 7, 8, 3]
# k_fold_executor_7 = [0, 1, 9, 3, 4, 5, 6, 7, 8, 2]
# k_fold_executor_8 = [0, 9, 2, 3, 4, 5, 6, 7, 8, 1]
# k_fold_executor_9 = [9, 1, 2, 3, 4, 5, 6, 7, 8, 0]


# run_kfold_validation(k_fold_executor_0, 9, generators)
# run_kfold_validation(k_fold_executor_1, 8, generators)
# run_kfold_validation(k_fold_executor_2, 7, generators)
# run_kfold_validation(k_fold_executor_4, 5, generators)
# run_kfold_validation(k_fold_executor_3, 6, generators)
# run_kfold_validation(k_fold_executor_5, 4, generators)
# run_kfold_validation(k_fold_executor_6, 3, generators)
# run_kfold_validation(k_fold_executor_7, 2, generators)
# run_kfold_validation(k_fold_executor_8, 1, generators)
# run_kfold_validation(k_fold_executor_9, 0, generators)

loss = resultado[0]/10
acc = resultado[1]/10
with open('resultado.txt', 'w') as arquivo:
    arquivo.write(str(loss)+';'+str(acc)+'\n')

print("+++++++++++++++++++++++++++++++++++++++++++ RESULTADO: ", resultado)