import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="1"

import pandas as pd
import os
import numpy as np
import time
import tensorflow.keras.initializers
import statistics
import tensorflow.keras
from sklearn import metrics
from sklearn.model_selection import StratifiedKFold
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout, InputLayer, Flatten
from tensorflow.keras import regularizers
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import StratifiedShuffleSplit
from tensorflow.keras.layers import LeakyReLU,PReLU
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications import VGG16
from tensorflow.keras.models import load_model, Model
from colabNotebook_kfold_10 import fit_generator, get_generator, evaluate
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import load_model, Model
from tensorflow.keras import optimizers


def generate_model_sysu (dropout, neuronPct, neuronShrink):
    image_size = 256
    batch_size_train = 10
    batch_size_validacao = 10
    epochs_ = 30
    neuronCount = int(neuronPct * 5000)
    base_model = VGG16(weights='imagenet', include_top=False, input_shape=(image_size, image_size, 3))
    #model =Sequential()
    x = Flatten(name='flatten')(base_model.output)
    
    layer = 0
    while neuronCount>25 and layer<10:
        # The first (0th) layer needs an input input_dim(neuronCount)
        x = Dropout(dropout)(x)
        if layer==0:
             x = Dense(1024, activation='relu', name='fc1')(x) 
   
        else:
            x = Dense(neuronCount, activation=PReLU()) (x)
        layer += 1

        # Add dropout after each hidden layer
        

        # Shrink neuron count for each layer
        neuronCount = neuronCount * neuronShrink
   
    x = Dense(3, activation='softmax', name='predictions')(x)
    model = Model(inputs=base_model.input, outputs=x)
    model.summary()
    
    return model

def evaluate_network(dropout,lr,neuronPct,neuronShrink):

    # Bootstrap
   
    dir_0 = 'dataset/treino/'
    dir_1 = 'dataset/teste/'

    train_datagen = ImageDataGenerator(
                    samplewise_center=True,
                    samplewise_std_normalization=True,
                    zoom_range=[0.8, 1.2],
                    horizontal_flip=True,
                    rotation_range=2,
                    brightness_range=[0.2, 1.0]
    )
    generator_treino = get_generator(dir_0, train_datagen)
    generator_avaliacao = get_generator(dir_1, train_datagen)

    # Track progress
    mean_benchmark = []
    epochs_needed = []
    num = 0
    



    model = generate_model_sysu(dropout, neuronPct, neuronShrink)
    model.compile(loss='categorical_crossentropy',
              optimizer=optimizers.RMSprop(lr=lr),
              metrics=['acc'])
    
    monitor = EarlyStopping(monitor='val_loss', min_delta=1e-3, 
    patience=100, verbose=0, mode='auto', restore_best_weights=True)
    # Train on the bootstrap sample
    
    model = fit_generator(generator_treino, model, generator_avaliacao, epochs=1000)

    



    epochs = monitor.stopped_epoch
    epochs_needed.append(epochs)
    # Predict on the out of boot (validation)
    
    score = evaluate(generator_avaliacao, model)

    tensorflow.keras.backend.clear_session()
    return (-score[0])

print(evaluate_network(
    dropout=0.2,
    lr=1e-3,
    neuronPct=0.2,
    neuronShrink=0.2))

from bayes_opt import BayesianOptimization
import time



# Supress NaN warnings, see: https://stackoverflow.com/questions/34955158/what-might-be-the-cause-of-invalid-value-encountered-in-less-equal-in-numpy
import warnings
warnings.filterwarnings("ignore",category =RuntimeWarning)

# Bounded region of parameter space
pbounds = {'dropout': (0.0, 0.499),
           'lr': (0.0, 0.1),
           'neuronPct': (0.01, 1),
           'neuronShrink': (0.01, 1)
          }

optimizer = BayesianOptimization(
    f=evaluate_network,
    pbounds=pbounds,
    verbose=2,  # verbose = 1 prints only when a maximum is observed, verbose = 0 is silent
    random_state=1,
)

optimizer.maximize(init_points=10, n_iter=100,)
print(optimizer.max)

"""{'target': -0.6500334282952827, 'params': {'dropout': 0.12771198428037775, 'lr': 0.0074010841641111965, 'neuronPct': 0.10774655638231533, 'neuronShrink': 0.2784788676498257}}"""
